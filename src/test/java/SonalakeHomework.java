import org.junit.Test;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;

import pageobjects.Calculator;

public class SonalakeHomework {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() throws Exception {
        String browser = System.getProperty("browser");
        switch (browser){
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "chrome":
                driver = new ChromeDriver();
                break;
            default:
                throw new Exception("Invalid browser");
        }
    }

    @Test
    public void testSteps() {
        Calculator calculator = PageFactory.initElements(driver, Calculator.class);

        // Step 1
        calculator.navigate();

        // Step 2
        calculator.clickThree();
        calculator.clickFive();
        calculator.clickMultiplication();
        calculator.clickNine();
        calculator.clickNine();
        calculator.clickNine();
        calculator.clickPlus();
        calculator.clickParenthesisLeft();
        calculator.clickOne();
        calculator.clickZero();
        calculator.clickZero();
        calculator.clickDivion();
        calculator.clickFour();
        calculator.clickParenthesisRight();
        calculator.clickEquals();

        assertEquals("34990", calculator.getDisplayText());
        calculator.clickClear();

        // Step 3
        calculator.chooseRadians();
        calculator.clickCosinus();
        calculator.clickPi();
        calculator.clickParenthesisRight();
        calculator.clickEquals();

        assertEquals("-1", calculator.getDisplayText());
        calculator.clickClear();

        // Step 4
        calculator.clickSqrt();
        calculator.clickEight();
        calculator.clickOne();
        calculator.clickParenthesisRight();
        calculator.clickEquals();

        assertEquals("9", calculator.getDisplayText());

        // Step 5
        List<String> historyEntries = calculator.getHistoryEntries();
        List<String> enteredEquations = Arrays.asList("35*999+(100/4)", "cos(pi)", "sqrt(81)");
        assertTrue(historyEntries.containsAll(enteredEquations));
    }

    @AfterClass
    public static void tearDown() {
        if (driver != null){
            driver.quit();
        }
    }
}
