package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private final static String URL = "https://web2.0calc.com/";
    private WebDriver driver;

    @FindBy(how = How.ID, using = "Btn0")
    WebElement zero;

    @FindBy(how = How.ID, using = "Btn1")
    WebElement one;

    @FindBy(how = How.ID, using = "Btn3")
    WebElement three;

    @FindBy(how = How.ID, using = "Btn4")
    WebElement four;

    @FindBy(how = How.ID, using = "Btn5")
    WebElement five;

    @FindBy(how = How.ID, using = "Btn8")
    WebElement eight;

    @FindBy(how = How.ID, using = "Btn9")
    WebElement nine;

    @FindBy(how = How.ID, using = "BtnPi")
    WebElement pi;

    @FindBy(how = How.ID, using = "BtnPlus")
    WebElement plus;

    @FindBy(how = How.ID, using = "BtnMult")
    WebElement multiplication;

    @FindBy(how = How.ID, using = "BtnDiv")
    WebElement division;

    @FindBy(how = How.ID, using = "BtnSqrt")
    WebElement sqrt;

    @FindBy(how = How.ID, using = "BtnCos")
    WebElement cosinus;

    @FindBy(how = How.ID, using = "BtnParanL")
    WebElement parenthesisLeft;

    @FindBy(how = How.ID, using = "BtnParanR")
    WebElement parenthesisRight;

    @FindBy(how = How.ID, using = "BtnCalc")
    WebElement calculate;

    @FindBy(how = How.ID, using = "BtnClear")
    WebElement clear;

    @FindBy(how = How.ID, using = "trigorad")
    WebElement radians;

    @FindBy(how = How.CSS, using =  "button[class='btn dropdown-toggle pull-right']")
    WebElement historyDropdown;

    @FindBy(how = How.XPATH, using = "//div[@id='histframe']//p[@class='l']")
    List<WebElement> historyFrameElements;

    @FindBy(how = How.ID, using = "input")
    WebElement display;

    public Calculator(WebDriver d){
        driver = d;
    }

    public void navigate(){
        driver.get(URL);
    }

    public void clickZero(){
        zero.click();
    }

    public void clickOne(){
        one.click();
    }

    public void clickThree(){
        three.click();
    }

    public void clickFour(){
        four.click();
    }

    public void clickFive(){
        five.click();
    }

    public void clickEight(){
        eight.click();
    }

    public void clickNine(){
        nine.click();
    }

    public void clickPi(){
        pi.click();
    }

    public void clickPlus(){
        plus.click();
    }

    public void clickMultiplication(){
        multiplication.click();
    }

    public void clickDivion(){
        division.click();
    }

    public void clickSqrt(){
        sqrt.click();
    }

    public void clickCosinus(){
        cosinus.click();
    }

    public void clickParenthesisLeft(){
        parenthesisLeft.click();
    }

    public void clickParenthesisRight(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(parenthesisRight));
        parenthesisRight.click();
    }

    public void clickEquals(){
        String before = getDisplayText();
        calculate.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(d -> !getDisplayText().equals(before));
    }

    public void clickClear(){
        clear.click();
    }

    public void chooseRadians(){
        radians.click();
    }

    public List<String> getHistoryEntries(){
        historyDropdown.click();

        List<String> historyEntries = new ArrayList<>();
        for (WebElement e : historyFrameElements){
            historyEntries.add(e.getText());
        }
        return historyEntries;
    }

    public String getDisplayText(){
        return display.getAttribute("value");
    }
}
